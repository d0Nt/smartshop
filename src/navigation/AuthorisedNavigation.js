import Settings from './SettingsNavigation';
import ShoppingCart from './../screens/ShoppingCart';
import PurchaseHistory from './../screens/PurchaseHistory';
import {createDrawerNavigator, DrawerItems} from 'react-navigation';
import React from 'react';
import { View, SafeAreaView, Alert } from 'react-native';
import Account from './../controllers/Account';
import Button from './../components/Button';
import colors from './../configs/colors';
import ProfilePhoto from '../components/ProfilePhoto';
import ProfileName from '../components/ProfileName';

export default createDrawerNavigator({ 
  ShoppingCart: ShoppingCart, 
  PurchaseHistory: PurchaseHistory,
  Settings: {
    screen: Settings,
    navigationOptions: ({ navigation }) => ({
        title: 'Nustatymai'
    })
  }
},
{
  contentComponent:(props) => (
    <View style={{flex:1, backgroundColor: colors.secondary, paddingTop: 25}}>
        <ProfilePhoto style={{
          marginBottom: 10,
          marginTop: 10,
          width: 100, 
          height: 100, 
          borderRadius: 100 / 2
          }} />
        <ProfileName />
        <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
          <DrawerItems {...props} />
          {additionalButtonsBottom(props)}
        </SafeAreaView>
    </View>
  ),
  contentOptions: {
    activeTintColor: colors.link,
    activeBackgroundColor: colors.primary,
    inactiveTintColor: colors.textColor,
    inactiveBackgroundColor: 'transparent',
    labelStyle: {
      color: colors.textColor,
    },
  },
  drawerOpenRoute: 'DrawerOpen',
  drawerCloseRoute: 'DrawerClose',
  drawerToggleRoute: 'DrawerToggle'
});

additionalButtonsBottom = (props) =>{
  return (
  <Button titleStyle={{color: colors.textColor}} onPress={()=>
    Alert.alert(
      'Atsijungimas',
      'Ar tikrai norite atsijungti?',
      [
        {text: 'Ne', onPress: () => {return null}},
        {text: 'Taip', onPress: () => {
          Account.logout();
          props.navigation.navigate('Auth');
        }},
      ],
      { cancelable: false }
    )  
  } title="Atsijungti" />
  );
}