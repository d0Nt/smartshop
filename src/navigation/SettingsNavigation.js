import Settings from './../screens/Settings';
import ChangePassword from './../screens/ChangePassword';
import DeleteRegistration from './../screens/DeleteRegistration';

import {createStackNavigator} from 'react-navigation';

export default createStackNavigator({ 
  Settings: Settings,
  ChangePassword: ChangePassword,
  DeleteRegistration: DeleteRegistration
});