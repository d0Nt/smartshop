import RegisterPage from './../screens/Register';

import {createStackNavigator} from 'react-navigation';
import Terms from './../screens/Terms';

export default createStackNavigator({ 
  Register: RegisterPage, 
  Terms: Terms 
});