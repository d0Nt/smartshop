import React from 'react';
import { View, SafeAreaView } from 'react-native';
import Login from './../screens/Login';
import Register from './../navigation/RegistrationNavigation';
import RemindPassword from './../screens/RemindPassword';
import {createDrawerNavigator, DrawerItems} from 'react-navigation';
import colors from './../configs/colors';

const nav = createDrawerNavigator({
        Login: {screen: Login},
        Register: {
            screen: Register,
            navigationOptions: ({ navigation }) => ({
                title: 'Registracija'
            })
        },
        RemindPassword: {screen: RemindPassword}
    }, {
        contentComponent:(props) => (
            <View style={{flex:1, backgroundColor: colors.secondary, paddingTop: 25}}>
                <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
                  <DrawerItems {...props} />
                </SafeAreaView>
            </View>
        ),
        contentOptions: {
            activeTintColor: colors.link,
            activeBackgroundColor: colors.primary,
            inactiveTintColor: colors.textColor,
            inactiveBackgroundColor: 'transparent',
            labelStyle: {
              color: colors.textColor,
            }
        },
        drawerPosition: 'left'
    });

nav.navigationOptions = () => ({
    title: 'Registracija',
});
export default nav;