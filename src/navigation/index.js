import Loading from './../screens/LoadingScreen';
import ErrorScreen from './../screens/ErrorScreen';
import UnAuthorisedNavigation from './UnAuthorisedNavigation';
import AuthorisedNavigation from './AuthorisedNavigation'
import { createSwitchNavigator, createAppContainer } from 'react-navigation';

export default createAppContainer(createSwitchNavigator(
  {
    Loading: Loading,
    App: AuthorisedNavigation,
    Auth: UnAuthorisedNavigation,
    Error: ErrorScreen
  },
  {
    initialRouteName: 'Loading',
  }
));
