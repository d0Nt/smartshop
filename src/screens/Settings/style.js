import { StyleSheet } from 'react-native';
import colors from '../../configs/colors';

export default StyleSheet.create({
    container: {
      padding: 20
    },
    body: {
      backgroundColor: colors.primary
    },
    contentText: {
      color: colors.textColor
    },
    itemRow:{
      height: 40,
      color: '#ffffff',
      alignItems: 'center',
      borderBottomWidth: 1,
      backgroundColor: 'rgba(255,255,255,.08)',
      borderBottomColor: 'rgba(255,255,255,.3)'
    },
    itemCol:{
      alignItems: 'center',
      justifyContent: 'center'
    },
    settingTitle:{
      color: '#8F8F98',
      textTransform: 'uppercase',
      fontSize: 13
    },
    settingContainer: {
      borderBottomColor: 'rgba(143, 143, 152, .4)',
      borderBottomWidth: 1,
      paddingBottom: 10,
      marginBottom: 10
    },
    input:{
      color: colors.textColor
    },
    button: {
      marginBottom: 10,
      width: '100%',
      backgroundColor: 'rgba(143, 143, 152, .4)',
      justifyContent: 'center',
      alignItems: 'center',
    },
    buttonSave:{
      backgroundColor: colors.link
    },
    avatarContainer:{
      justifyContent: 'center',
      alignItems: 'center',
    },
    avatar: {
      marginBottom: 20,
      width: 150, 
      height: 150, 
      borderRadius: 150 / 2 
    },
    text: {
      color: colors.textColor
    }
  });
  