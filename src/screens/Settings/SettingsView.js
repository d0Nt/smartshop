import React from 'react';
import style from './style';
import { Text, TextInput, ScrollView, View } from 'react-native';
import Header from './../../components/Header';
import { Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';
import Button from '../../components/Button';
import ProfilePhoto from './../../components/ProfilePhoto';

const Settings = ({
    email, setEmail,
    firstName, setFirstName,
    lastName, setLastName,
    saving,
    changePassword, submit,
    deleteRegistration
}) => (
<Grid>
    <Row size={sizes.headerHeight}>
        <Header title='Nustatymai' drawerToggle={true} initial={true} nav={navigation}/>
    </Row>
    <Row style={style.body} size={sizes.bodyHeight}>
        <ScrollView contentContainerStyle={style.container}>
            <ProfilePhoto />

            <View style={style.settingContainer}>
                <Text style={style.settingTitle}>VARDAS</Text>
                <TextInput style={style.input} 
                onChangeText={setFirstName} placeholder='Vardas' value={firstName}/>
            </View>

            <View style={style.settingContainer}>
                <Text style={style.settingTitle}>PAVARDĖ</Text>
                <TextInput style={style.input} 
                onChangeText={setLastName} placeholder='Pavardė' value={lastName}/>
            </View>
             
            <View style={style.settingContainer}>
                <Text style={style.settingTitle}>EL. PAŠTAS</Text>
                <TextInput style={style.input} 
                onChangeText={setEmail} placeholder='example@smartshop.lt' value={email}/>
            </View>
            <Button onPress={submit} style={[style.button, style.buttonSave]} titleStyle={style.text} title="Išsaugoti" />

            <Button disabled={saving} onPress={changePassword} style={style.button} 
            titleStyle={style.text} title="Pakeisti slaptažodį" />

            <Button onPress={deleteRegistration} style={[style.button,{
                backgroundColor: '#DB4514',
                marginTop: 40
            }]} 
            titleStyle={style.text} title="Ištrinti registraciją" />
        </ScrollView>
    </Row>
</Grid>
);

Settings.navigationOptions = () => ({
    title: 'Nustatymai',
    header: null
});

export default Settings;