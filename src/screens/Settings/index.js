import { compose, hoistStatics, withHandlers, withProps, withState, lifecycle } from 'recompose';
import Settings from './SettingsView';
import message from './../../components/ToastMessage';
import userVariables from './../../controllers/userVariables';
import Account from '../../controllers/Account';

const enhancer = compose(
  withState('email', 'setEmail', ''),
  withState('firstName', 'setFirstName', ''),
  withState('lastName', 'setLastName', ''),
  withState('saving', 'setSavingState', false),
  withProps(props => {
    navigation = props.navigation
  }),
  lifecycle({
    componentWillMount(){
      let user = userVariables.getData();
      this.props.setEmail(user.email);
      this.props.setLastName(user.lastName);
      this.props.setFirstName(user.firstName);
    }
  }),
  withHandlers({
      changePassword: props => () => props.navigation.navigate("ChangePassword"),
      deleteRegistration: props => () => props.navigation.navigate("DeleteRegistration"),
      submit: props => async () => {
        if(props.saving) return;
        let user = userVariables.getData();
        let saveArray = {};
        if(props.email !== user.email){
          if(!Account.validateMail(props.email)){
            message("El. pašto adresas neteisingas.");
            return;
          }
          saveArray.email = props.email;
        }
        if(props.firstName !== user.firstName){
          if(props.firstName == '' || props.firstName.length < 2){
            message("Vardo laukas negali būti tuščias.");
            return;
          }
          saveArray.firstName = props.firstName;
        }
        if(props.lastName !== user.lastName){
          if(props.lastName == '' || props.lastName.length < 2){
            message("Pavardės laukas negali būti tuščias.");
            return;
          }
          saveArray.lastName = props.lastName;
        }
        if(Object.entries(saveArray).length != 0){
          props.setSavingState(true);
          await Account.saveUserData(saveArray);
          props.setSavingState(false);
          message("Informacija išsaugota.");
        }else message("Informacija nebuvo pakeista.");
      }
  })
);
export default hoistStatics(enhancer)(Settings);
