import { StyleSheet } from 'react-native';
import colors from './../../configs/colors.dark';

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.primary,
      alignItems: 'center',
      color: colors.textColor,
      justifyContent: 'center',
    },
    body: {
      backgroundColor: colors.primary,
    },
    itemRow:{
      height: 40,
      color: '#ffffff',
      alignItems: 'center',
      borderBottomWidth: 1,
      backgroundColor: 'rgba(255,255,255,.08)',
      borderBottomColor: 'rgba(255,255,255,.3)'
    },
    rowText: {
      color: colors.textColor,
      alignItems: 'center'
    },
    itemCol:{
      alignItems: 'center',
      justifyContent: 'center'
    },
    buttons:{
      flex: 1,
      marginTop: 5,
      alignItems: 'center',
      paddingHorizontal: 10,
      color: '#ffffff',
      justifyContent: 'center',
    },
    whiteText:{
      color: colors.textColor
    },
  });
  