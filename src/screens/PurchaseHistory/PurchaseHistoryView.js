import React from 'react';
import style from './style';
import sizes from './../../configs/sizes';
import { Text, View, ScrollView } from 'react-native';
import Header from './../../components/Header';
import { Col, Row, Grid } from "react-native-easy-grid";
import MaterialIcons from '@expo/vector-icons/MaterialIcons';
import SpinningIcon from '../../components/SpinningIcon';
import Dialog from '../../components/Dialog';
import Button from '../../components/Button';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';

const PurchaseHistory = ({
    viewCartData,
    detailsShown,
    showDetails
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header title="Apsipirkimų istorija" nav={navigation}/>
        </Row>
        <Row style={style.body} size={sizes.bodyHeight}>
        {cartDataDialog(detailsShown, showDetails)}
        {cartsData === null && (
            <View style={style.container}>
                <SpinningIcon name="spinner" size={128} style={{ 
                    color: '#28D8A1'
                }} />
            </View>
        )}
        {cartsData !== null && (
            <ScrollView contentContainerStyle={style.scrollableContainer}>
                <Row  style={style.itemRow}>
                    <Col size={70} style={style.itemCol}><Text style={style.rowText}>Data</Text></Col>
                    <Col size={20} style={style.itemCol}><Text style={style.rowText}>Kaina</Text></Col>
                    <Col size={10} style={style.itemCol}></Col>
                </Row>
                {renderHisotryItems(viewCartData)}
            </ScrollView>
        )}
        </Row>
    </Grid>
);
getCartItemsByID = (id) => {
    let found = cartsData.find( function(item) { return item.id == id } );
    if(typeof found === 'undefined') return {items: []};
    return found.items;
}
cartDataDialog = (visible, ti) => {
    return (
    <Dialog visible={visible} onRequestClose={()=>ti(false)}
    style={{
        width: '90%',
        minHeight: '90%'
    }}>
    <Grid>
        <Row>
            <ScrollView contentContainerStyle={style.scrollableContainer}>
                <Row style={style.itemRow}>
                    <Col size={15} style={style.itemCol}><Text style={style.rowText}>Kiekis</Text></Col>
                    <Col size={65} style={style.itemCol}><Text style={style.rowText}>Produkto pavadinimas</Text></Col>
                    <Col size={20} style={style.itemCol}><Text style={style.rowText}>Kaina</Text></Col>
                </Row>
                {cartDetails !== null && renderCartDetails()}
            </ScrollView>
        </Row>
        <Row style={{height: 80}}>
            <Button style={style.buttons} onPress={()=>ti(false)}>
                <MaterialCommunityIcons name="cancel" size={30} style={style.whiteText}/>
                <Text style={style.whiteText}>Uždaryti</Text>
            </Button>
        </Row>
    </Grid>
    </Dialog>
    );
}
renderCartDetails = () => {
    return cartDetails.map(itemInfo => (
        <Row key={itemInfo.id} style={style.itemRow}>
            <Col size={15} style={style.itemCol}><Text style={style.rowText}>{(itemInfo.amount).toString()}</Text></Col>
            <Col size={50} style={style.itemCol}><Text style={style.rowText}>{itemInfo.name}</Text></Col>
            <Col size={20} style={style.itemCol}><Text style={style.rowText}>{(itemInfo.price*itemInfo.amount).toFixed(2)} €</Text></Col>
        </Row>
    ));
}
renderHisotryItems = (viewCartData) => {
    return cartsData.map(cartData => (
        <Row key={cartData.id} style={style.itemRow}>
            <Col size={70} style={style.itemCol}><Text style={style.rowText}>{(cartData.paid).toString()}</Text></Col>
            <Col size={20} style={style.itemCol}><Text style={style.rowText}>{(cartData.price).toString()} €</Text></Col>
            <Col size={10} style={style.itemCol}>
                <MaterialIcons onPress={()=>viewCartData(getCartItemsByID(cartData.id))} name="pageview" size={24} style={{color: '#28D8A1'}} />
            </Col>
        </Row>
    ));
}

PurchaseHistory.navigationOptions = () => ({
    title: 'Apsipirkimų istorija',
});

export default PurchaseHistory;