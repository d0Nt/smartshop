import { compose, hoistStatics, withHandlers, withState, withProps, lifecycle } from 'recompose';
import PurchaseHistory from './PurchaseHistoryView';
import Shopping from './../../controllers/Shopping';

loadHistoryData = async (props) => {
  let historyData = await Shopping.purchaseHistory();
  props.setData(historyData);
}

viewCartDataDialog = (props, data) => {
  props.setCartDetails(data);
  props.showDetails(true);
}

const enhancer = compose(
  withState("data", "setData", null),
  withState("cartDetails", "setCartDetails", null),
  withState("detailsShown", "showDetails", false),
  withProps(props => {
    navigation = props.navigation,
    cartDetails = props.cartDetails,
    cartsData = props.data
  }),
  lifecycle({
    componentWillMount(){
      loadHistoryData(this.props);
    }
  }),
  withHandlers({
    viewCartData: props=> (data) => viewCartDataDialog(props, data)
  })
);
export default hoistStatics(enhancer)(PurchaseHistory);
