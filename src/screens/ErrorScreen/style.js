import {StyleSheet} from 'react-native';
import colors from './../../configs/colors.dark.js';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.primary,
        alignItems: 'center',
        color: colors.textColor
    },
    icon:{
        color: '#e25f5f'
    },
    errorTitle:{
        color:colors.textColor,
        fontSize: 30,
        marginTop: 20
    },
    errorMessage: {
        color:colors.textColor,
        fontSize: 20,
        paddingHorizontal: 20
    }
});