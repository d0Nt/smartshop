import { compose, hoistStatics, withProps, withHandlers } from 'recompose';
import ErrorView from './ErrorView';

const enhancer = compose(
    withProps(props => {
        navigation = props.navigation,
        message = (
            props.navigation.state.params?
            props.navigation.state.params.message:
            "Klaida.."),
        restartButton = (!props.navigation.state.params)?false:props.navigation.state.params.restartAllowed
    }),
    withHandlers({
        reloadApp: props => () => {
            props.navigation.navigate('Loading');
        }
    })
);

export default hoistStatics(enhancer)(ErrorView);