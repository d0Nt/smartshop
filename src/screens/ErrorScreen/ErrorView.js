import React from 'react';
import { View, Text} from 'react-native';
import sizes from '../../configs/sizes';
import Header from '../../components/Header';
import { Row, Grid } from "react-native-easy-grid";
import { FontAwesome } from '@expo/vector-icons';
import styles from './style';
import Button from '../../components/Button';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';

const ErrorView = ({
    reloadApp
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header />
        </Row>
        <Row size={sizes.bodyHeight}>
            <View style={styles.container}>
                <FontAwesome name="warning" size={128} style={styles.icon} />
                <Text style={styles.errorTitle}>Įvyko klaida</Text>
                <Text style={styles.errorMessage}>{message}</Text>
                {restartButton && (
                    <Button onPress={reloadApp} style={{
                        marginTop: 20
                    }}>
                        <MaterialCommunityIcons style={styles.icon} name='restart' size={42}/>
                    </Button>
                )}
            </View>
        </Row>
    </Grid>
);
export default ErrorView;