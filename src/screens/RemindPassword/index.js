import { compose, hoistStatics, withHandlers, withProps } from 'recompose';
import RemindPassword from './RemindPasswordView';
import message from './../../components/ToastMessage';
import Account from './../../controllers/Account';

remindPassword = async (data, navigation) => {
  if(!data.email)
  {
    message("El. pašto adresas negali būti tuščias.");
    return;
  }
  if(!Account.validateMail(data.email))
  {
    message("El. pašto adresas neteisingas.");
    return;
  }
  let response = await Account.resetPassword(data.email);
  if(response == null){
    message("Nepavyko pasiekti API.");
    return;
  }
  if(response)
    message("Slaptažodžio atstatymo patvirtinimas išsiųstas nurodytų el. paštu.");
  else
    message("Registracija su tokiu el. paštu nerasta.");
}

const enhancer = compose(
  withProps(props => {
    navigation = props.navigation,
    fields = {
      email: ''
    }
  }),
  withHandlers({
    remindPassword: () => remindPassword,
  })
);
export default hoistStatics(enhancer)(RemindPassword);