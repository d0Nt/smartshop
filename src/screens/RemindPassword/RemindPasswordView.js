import React from 'react';
import T from 'prop-types';
import styles from './style';
import { Text, View, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import ImageLoader from './../../components/ImageLoader';
import Header from './../../components/Header';
import { Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';

const RemindPassword = ({
    remindPassword
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header nav={navigation}/>
        </Row>
        <Row size={sizes.bodyHeight}>
        <View style={styles.container}>
            <ImageLoader source={require('./../../../assets/logo.png')} 
                    style={styles.logo} />
            <Text style={styles.text}>
                Norint atstatyti slaptažodį reikia nurodyti jau egzistuojančios registracijos el. pašto adresą žemiau nurodytame lauke.
            </Text>
                
            <Text style={styles.inputLabel}>El. paštas:</Text>
            <TextInput style={styles.input} onChangeText={(val) => fields.email = val} placeholder='example@smartshop.lt'/>

            <TouchableOpacity style={[styles.button, styles.loginButton]} onPress={() => remindPassword(fields, navigation)}>
                <Text style={styles.buttonText}>Priminti</Text>
            </TouchableOpacity>
        </View>
        </Row>
    </Grid>
);

RemindPassword.propTypes = {
    remindPassword: T.func
};

RemindPassword.navigationOptions = () => ({
    title: 'Slaptažodžio priminimas',
});

export default RemindPassword;