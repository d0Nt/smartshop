import { compose, hoistStatics, withHandlers, withProps } from 'recompose';
import Register from './RegisterView';
import Account from './../../controllers/Account';
import message from './../../components/ToastMessage';

register = async (data, navigation) => {
  if(data.firstName == '' || data.firstName.length < 2){
    message("Vardo laukas negali būti tuščias.");
    return;
  }
  if(data.lastName == '' || data.lastName.length < 2){
      message("Pavardės laukas negali būti tuščias.");
      return;
  }
  if(data.email == ''){
      message("El. pašto laukas negali būti tuščias.");
      return;
  }
  if(!Account.validateMail(data.email)){
      message("El. pašto adresas neteisingas.");
      return;
  }
  if(data.password == ''){
      message("Slaptažodžio laukas negali būti tuščias.");
      return;
  }
  if(!Account.validatePassword(data.password)){
      message("Slaptažodis turi būti bent 6 simbolių ilgio, jame turi būti viena didžioji, viena mažoji raidė ir simbolis arba skaičius.");
      return;
  }
  if(data.password != data.passwordAgain){
      message("Slaptažodžiai nesutampa.");
      return;
  }
  let registered = await Account.register(data);
  if(registered == null){
    message("Nepavyko pasiekti API.");
    return;
  }
  if(!registered)
    message(registered);
  else{
    message("Registracija sėkminga.");
    navigation.navigate('Login');
  }
}

const enhancer = compose(
  withProps(props => {
    navigation = props.navigation,
    fields ={
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      passwordAgain: ''
    }
  }),
  withHandlers({
    register: () => register,
  })
);

export default hoistStatics(enhancer)(Register);