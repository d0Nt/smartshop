import React from 'react';
import T from 'prop-types';
import styles from './style';
import { Text, ScrollView, View, TouchableOpacity, TextInput } from 'react-native';
import ImageLoader from './../../components/ImageLoader';
import Header from './../../components/Header';
import { Col, Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';

const Register = ({
    register
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header nav={navigation}/>
        </Row>
        <Row size={sizes.bodyHeight}>
            <ScrollView contentContainerStyle={styles.scrollContainer}>
                <ImageLoader source={require('./../../../assets/logo.png')} style={styles.logo} />

                <Text style={styles.inputLabel}>Vardas:</Text>
                <TextInput style={styles.input} onChangeText={(val) => fields.firstName = val} placeholder='Vardenis'/>

                <Text style={styles.inputLabel}>Pavardė:</Text>
                <TextInput style={styles.input} onChangeText={(val) => fields.lastName = val} placeholder='Pavardenis'/>

                <Text style={styles.inputLabel}>El. paštas:</Text>
                <TextInput style={styles.input} onChangeText={(val) => fields.email = val} placeholder='example@smartshop.lt'/>

                <Text style={styles.inputLabel}>Slaptažodis:</Text>
                <TextInput style={styles.input} onChangeText={(val) => fields.password = val} secureTextEntry={true} placeholder='Slaptažodis'/>
                
                <Text style={styles.inputLabel}>Pakartokite slaptažodį:</Text>
                <TextInput style={styles.input} onChangeText={(val) => fields.passwordAgain = val} secureTextEntry={true} placeholder='Slaptažodis'/>

                <View style={styles.terms}>
                    <Text style={styles.text}>
                        Registruodamiesi sutinkate su mūsų<Text onPress={()=> navigation.navigate("Terms")} style={styles.link}> naudojimosi taisyklėmis</Text>
                    </Text>
                </View>

                <TouchableOpacity style={[styles.button, styles.loginButton]} onPress={() => register(fields, navigation)}>
                    <Text style={styles.buttonText}>Registruotis</Text>
                </TouchableOpacity>
            </ScrollView>
        </Row>
    </Grid>
);

Register.propTypes = {
    register: T.func
};

Register.navigationOptions = () => ({
    title: 'Registracija',
    header: null
});

export default Register;