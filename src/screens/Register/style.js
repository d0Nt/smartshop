import colors from './../../configs/colors.dark';
import { StyleSheet, Dimensions } from 'react-native';

let width = Dimensions.get('window').width;

export default StyleSheet.create({
    scrollContainer:{
      backgroundColor: colors.primary,
      alignItems: 'center',
      paddingBottom: 30
    },
    logo: {
      width: 150,
      height: 150,
      resizeMode: "cover"
    },
    input: {
      width: width - 60,
      borderBottomColor: '#687989',
      backgroundColor: 'rgba(255,255,255,.1)',
      borderBottomWidth: 2,
      color: colors.textColor,
      marginBottom: 20,
      padding: 5
    },
    inputLabel:{
      fontSize: 18,
      color: colors.textColor,
      marginBottom: 10
    },
    buttonText:{
      color: colors.textColor,
    }, 
    button:{
      width: width - 60,
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      fontSize: 15,
      backgroundColor: '#ffffff',
      marginBottom: 10
    },
    loginButton:{
      marginTop: 10,
      alignItems: 'center',
      justifyContent: 'center',
      padding: 10,
      fontSize: 15,
      backgroundColor: colors.link
    },
    link:{
      color: colors.link
    },
    text:{
      color: colors.textColor
    },
    terms:{
      marginHorizontal: 30,
      marginBottom: 10,
      justifyContent: 'center',
      alignItems: 'center',
    }
  });
  