import { compose, hoistStatics, withHandlers, withState, withProps, lifecycle } from 'recompose';
import Login from './LoginView';
import message from './../../components/ToastMessage';
import Account from './../../controllers/Account';

login = async (data, navigation) => {
  if(data.email == ''){
    message("El. pašto laukas negali būti tuščias.");
    return;
  }
  if(!Account.validateMail(data.email)){
    message("El. pašto adresas neteisingas.");
      return;
  }
  if(data.password == ''){
    message("Slaptažodžio laukas negali būti tuščias.");
      return;
  }
  let logged = await Account.login(data.email, data.password);
  if(logged == null){
    message("Nepavyko pasiekti API.");
    return;
  }
  if(logged){
    navigation.navigate('App');
    await Account.getData();
  } else
    message("Slaptažodis arba vartotojo vardas neteisingas.");
}

const enhancer = compose(
  withState("visible", "setVisible", false),
  withProps(props => {
    navigation = props.navigation,
    fields = {
      email: '',
      password: ''
    }
  }),
  lifecycle({
    componentDidMount(){
      this.props.setVisible(true);
    }
  }),
  withHandlers({
    registerPage: props => () => props.navigation.navigate('Register'),
    remindPasswordPage: props => () => props.navigation.navigate('RemindPassword'),
    signIn: () => login
  })
);
export default hoistStatics(enhancer)(Login);
