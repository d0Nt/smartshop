import React from 'react';
import T from 'prop-types';
import styles from './style';
import { Text, View, TextInput, TouchableOpacity } from 'react-native';
import Fade from './../../components/Fade';
import ImageLoader from './../../components/ImageLoader';
import Header from './../../components/Header';
import { Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';
import Button from '../../components/Button';

const Login = ({
    registerPage,
    remindPasswordPage,
    signIn,
    visible
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header initial={true} nav={navigation}/>
        </Row>
        <Row size={sizes.bodyHeight}>
            <View style={styles.container}>
                <Fade style={styles.center} visible={visible}>
                    <ImageLoader source={require('./../../../assets/logo.png')} 
                                style={styles.logo} />
                    <Text style={styles.inputLabel}>El. paštas:</Text>
                    <TextInput style={styles.input} onChangeText={(val) => fields.email = val} placeholder='example@smartshop.lt'/>

                    <Text style={styles.inputLabel}>Slaptažodis:</Text>
                    <TextInput style={styles.input} onChangeText={(val) => fields.password = val} secureTextEntry={true} placeholder='Slaptažodis'/>
                    
                    <Button title="Prisijungti" titleStyle={styles.text} style={[styles.button, styles.loginButton]} onPress={() => signIn(fields, navigation)} />

                    <View style={styles.center}>
                        <Text style={styles.text}>
                            Pamiršai slaptažodį?<Text onPress={remindPasswordPage} style={styles.link}> Atstatyk jį čia!</Text>
                        </Text>
                    </View>

                    <View style={[styles.bottomScreen, styles.center]}>
                        <Text style={styles.text}>
                            Dar neturi registracijos?<Text onPress={registerPage} style={styles.link}> Užsiregistruok!</Text>
                        </Text>
                    </View>
                    
                </Fade>
            </View>
        </Row>
    </Grid>
);

Login.propTypes = {
    registerPage: T.func,
    remindPasswordPage: T.func,
    signIn: T.func
};

Login.navigationOptions = () => ({
    title: 'Prisijungimas',
});

export default Login;