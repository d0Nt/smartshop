import React from 'react';
import style from './style';
import { Text, View, TextInput, ScrollView } from 'react-native';
import { Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';
import Button from './../../components/Button';
import Header from './../../components/Header';

const ChangePassword = ({
    setCurrent,
    setNew,
    setNewAgain,
    submit
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header title='Slaptažodžio keitimas' nav={navigation}/>
        </Row>
        <Row style={style.body} size={sizes.bodyHeight}>
            <ScrollView contentContainerStyle={style.container}>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>REIKALAVIMAI NAUJAM SLAPTAŽODŽIUI:</Text>
                    <Text style={style.text}>• bent 6 simbolių ilgio;{"\n"}
                    • bent viena didžioji raidė;{"\n"}
                    • bent viena mažoji raidė;{"\n"}
                    • bent vienas simbolis arba skaičius.{"\n"}</Text>
                </View>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>DABARTINIS SLAPTAŽODIS</Text>
                    <TextInput style={style.input} onChangeText={(val) => setCurrent(val)} secureTextEntry={true} placeholder='Dabartinis'/>
                </View>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>NAUJAS SLAPTAŽODIS</Text>
                    <TextInput style={style.input} onChangeText={(val) => setNew(val)} secureTextEntry={true} placeholder='Naujas'/>
                </View>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>PAKARTOKITE NAUJĄ</Text>
                    <TextInput style={style.input} onChangeText={(val) => setNewAgain(val)} secureTextEntry={true} placeholder='Naujas dar kartą'/>
                </View>

                <Button titleStyle={style.text} title="Išsaugoti" style={[style.button, style.buttonSave]} onPress={submit}/>
            </ScrollView>
        </Row>
    </Grid>
);

ChangePassword.navigationOptions = () => ({
    title: 'Slaptažodžio keitimas',
    header: null
});

export default ChangePassword;