import { compose, hoistStatics, withHandlers, withProps, withState } from 'recompose';
import ChangePassword from './ChangePasswordView';
import message from './../../components/ToastMessage';
import Account from './../../controllers/Account';

const enhancer = compose(
  withState('saving', 'setSavingState', false),
  withState('current', 'setCurrent',''),
  withState('new', 'setNew',''),
  withState('newAgain', 'setNewAgain',''),
  withProps(props => {
    navigation = props.navigation
  }),
  withHandlers({
    submit: props => async () => {
      if(props.saving) return;
      if(props.current.length < 2){
        message("Neįvestas senas slaptažodis.");
        return;
      }
      if(!Account.validatePassword(props.new)){
        message("Naujas slaptažodis neatitinka reikalavimų.");
        return;
      }
      if(props.new !== props.newAgain){
        message("Nauji slaptažodžiai nesutampa.");
        return;
      }
      props.setSavingState(true);
      let response = await Account.changePassword(props.current, props.new, props.newAgain);
      props.setSavingState(false);
      if(response === true)
        message("Slaptažodis pakeistas.");
      else if(response === null)
        message("Nepavyko pakeisti slaptažodžio, pabandykite dar kartą.");
      else
        message("Senas slaptažodis neteisingas.");
    }
  })
);
export default hoistStatics(enhancer)(ChangePassword);
