import { compose, hoistStatics, withHandlers, withProps } from 'recompose';
import Terms from './TermsView';

const enhancer = compose(
  withProps(props => {}),
  withHandlers({})
);

export default hoistStatics(enhancer)(Terms);