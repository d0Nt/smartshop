import React from 'react';
import T from 'prop-types';
import style from './style';
import { Text, ScrollView, View } from 'react-native';
import Header from './../../components/Header';
import { Col, Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';

const Terms = ({
    
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header nav={navigation}/>
        </Row>
        <Row size={sizes.bodyHeight}>
            <ScrollView contentContainerStyle={style.scrollContainer}>
                <Text style={style.title}>Introduction</Text>
                <Text style={style.text}>
                    These Website Standard Terms and Conditions written on this webpage shall manage your use of our website, Webiste Name accessible at Website.com.
                    These Terms will be applied fully and affect to your use of this Website. By using this Website, you agreed to accept all terms and conditions written in here. You must not use this Website if you disagree with any of these Website Standard Terms and Conditions.
                    Minors or people below 18 years old are not allowed to use this Website.
                </Text>
                <Text style={style.title}>Intellectual Property Rights</Text>
                <Text style={style.text}>
                    Other than the content you own, under these Terms, Company Name and/or its licensors own all the intellectual property rights and materials contained in this Website.
                    You are granted limited license only for purposes of viewing the material contained on this Website.
                </Text>
                
                <Text style={style.title}>Restrictions</Text>
                <Text style={style.text}>
                    You are specifically restricted from all of the following:
                    publishing any Website material in any other media;
                    selling, sublicensing and/or otherwise commercializing any Website material;
                    publicly performing and/or showing any Website material;
                    using this Website in any way that is or may be damaging to this Website;
                    using this Website in any way that impacts user access to this Website;
                    using this Website contrary to applicable laws and regulations, or in any way may cause harm to the Website, or to any person or business entity;
                    engaging in any data mining, data harvesting, data extracting or any other similar activity in relation to this Website;
                    using this Website to engage in any advertising or marketing.
                    Certain areas of this Website are restricted from being access by you and Company Name may further restrict access by you to any areas of this Website, at any time, in absolute discretion. Any user ID and password you may have for this Website are confidential and you must maintain confidentiality as well.
                </Text>
            </ScrollView>
        </Row>
    </Grid>
);

Terms.propTypes = {
    
};

Terms.navigationOptions = () => ({
    title: 'Naudojimosi taisyklės',
    header: null
});

export default Terms;