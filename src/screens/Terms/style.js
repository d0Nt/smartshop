import colors from './../../configs/colors.dark';
import { StyleSheet, Dimensions } from 'react-native';

let width = Dimensions.get('window').width
let height = Dimensions.get('window').height

export default StyleSheet.create({
    container: {
      flex: 1,
      minHeight: height,
      backgroundColor: colors.primary,
      justifyContent: 'center',
      color: colors.textColor
    },
    scrollContainer:{
      backgroundColor: colors.primary,
      alignItems: 'center',
      justifyContent: 'center',
      paddingBottom: 30
    },
    title:{
      marginTop: 10,
      color: colors.textColor,
      fontSize: 24
    },
    text:{
      color: colors.textColor,
      fontSize: 18,
      marginHorizontal: 30
    }
  });
  