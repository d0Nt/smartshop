import {StyleSheet} from 'react-native';
import colors from './../../configs/colors.dark.js';

export default StyleSheet.create({
    container: {
        
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary        
    }
});