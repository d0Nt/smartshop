import React from 'react';
import { View } from 'react-native';
import { WaveIndicator } from 'react-native-indicators';
import colors from './../../configs/colors.dark.js';
import style from './style';
import ImageLoader from './../../components/ImageLoader';
import Fade from './../../components/Fade';

const Loading = ({visible}) => (
    <View style={style.container}>
        <Fade visible={visible}>
        <ImageLoader source={require('./../../../assets/logo.png')} 
            style={{
                marginTop: "25%",
                width: 150,
                height: 150,
                resizeMode: "cover"
            }}
        /> 
        <WaveIndicator color={colors.textColor} size={100} />
        </Fade>
    </View>
);

export default Loading;