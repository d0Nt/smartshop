import { compose, lifecycle, hoistStatics, withState } from 'recompose';
import Loading from './LoadingView';
import Account from './../../controllers/Account';

let loadingUser = async (loaded) =>{
    let userLoaded = await Account.restoreSession();
    await Account.getData();
    loaded(userLoaded);
}

const enhancer = compose(
    withState("visible", "setVisible", true),
     lifecycle({
        componentDidMount(){
            loadingUser(async (userLoaded) =>{
                this.props.setVisible(false);
                if(userLoaded === null)
                {
                    this.props.navigation.navigate("Error", {
                        "message": "Nepavyko prisijungti prie nuotolinio serverio.",
                        "restartAllowed": true
                    });
                    return;
                }
                this.props.navigation.navigate(userLoaded?'App':'Auth');
            });
        },
    })
);
export default hoistStatics(enhancer)(Loading);