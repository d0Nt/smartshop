import { compose, hoistStatics, lifecycle, withProps, withState, withHandlers } from 'recompose';
import ShoppingCart from './ShoppingView';
import Shopping from './../../controllers/Shopping';
import messasge from './../../components/ToastMessage';

loadData = async (props) => {
  let id = await Shopping.getActiveCart();
  let items = [];
  if(id > 0)
    items = await Shopping.getCartItems(id);
  props.setCart({
    id: id,
    items: items 
  });
}
removeCartItem = async (id, props) => {
  let items = await Shopping.removeItem(props.cart.id, id);
  props.setCart({
    id: props.cart.id,
    items: items 
  });
}
scanner = async (code, props) => {
  if(code === null){
    props.toggleCamera(false);
    return;
  }
  console.log(code);
  let request = await Shopping.addItem(code, props.cart.id);
  if(request.response == null){
    messasge("Nepavyko pasiekti API");
    return;
  }
  if(request.response == false){
    messasge(request.message);
    return;
  }
  props.setCart({
    id: props.cart.id,
    items: request.response 
  });
  props.toggleCamera(false);
} 
const enhancer = compose(
  withState("cart", "setCart", {
    id: -1,
    items: []
  }),
  withState("cameraToogled", "toggleCamera", false),
  withState("input", "toggleInput", false),
  withProps(props => {
    navigation = props.navigation
  }),
  lifecycle({
    componentDidMount(){
      loadData(this.props);
    }
  }),
  withHandlers({
      removeItem: props => (id) => removeCartItem(id, props),
      toggleCamera: props => (toggle) => props.toggleCamera(toggle),
      toggleInput: props => (toggle) => props.toggleInput(toggle),
      onCodeScanned: props => (data) => scanner(data, props),
      newCart: props =>  async() => {
        let cartId = await Shopping.createCart();
        props.setCart({
          id: cartId,
          items: [] 
        });
      },
      pay: props => async() => {
        let state = await Shopping.payForCart(props.cart.id);
        if(state.error){
          if(state.code != 'CART_PAID')
            return;
        }
        props.setCart({
          id: 0,
          items: []
        });
      }
  })
);
export default hoistStatics(enhancer)(ShoppingCart);
