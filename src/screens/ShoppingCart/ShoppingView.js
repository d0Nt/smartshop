import React from 'react';
import sizes from '../../configs/sizes';
import style from './style';
import { Row, Grid } from "react-native-easy-grid";
import Header from '../../components/Header';
import Cart from './Cart';
import Button from './../../components/Button';
import FontAwesome from '@expo/vector-icons/FontAwesome';
import { View, Text } from 'react-native';
import Camera from '../../components/Camera';
import Dialog from '../../components/Dialog';
import { TextInput } from 'react-native-gesture-handler';
import MaterialIcons from '@expo/vector-icons/MaterialIcons';
import MaterialCommunityIcons from '@expo/vector-icons/MaterialCommunityIcons';

let bottomLineHeigth = 10;
bodyheight = (cartId) =>{ 
    if(cartId > 0)
    return sizes.bodyHeight - bottomLineHeigth;
    else return sizes.bodyHeight;
}

const ShoppingCart = ({
    removeItem,
    cart,
    cameraToogled,
    toggleCamera,
    onCodeScanned,
    input,
    toggleInput,
    newCart,
    pay
}) => {
    if(cameraToogled)
        return (
            <Camera onCodeScanned={onCodeScanned}/>
        );
    else return (
        <Grid>
            <Row size={sizes.headerHeight}>
                <Header title="Pagrindinis" drawerToggle={true} initial={true} nav={navigation}/>
            </Row>
            <Row style={style.body} size={bodyheight(cart.id)}>

                {inputDialog(input, toggleInput, onCodeScanned)}

                <Cart startNewCart={newCart} removeItem={removeItem} cartItems={cart.items} cart={cart.id}/>
            </Row>
            {cart.id > 0 && footer(toggleCamera, toggleInput, pay)}
        </Grid>
    );
}

footer = (tc, ti, pay) => (
    <Row size={bottomLineHeigth}>
    <View style={style.footer}>
        <Button style={style.buttons} onPress={() => ti(true)}>
            <FontAwesome name="search" size={30} style={style.whiteText}/>
            <Text style={style.whiteText}>Įvesti kodą</Text>
        </Button>
        <Button onPress={() => tc(true)} style={[style.buttons,style.centerButton]} titleStyle={{margin:0}}>
            <FontAwesome name="qrcode" size={64} style={style.whiteText}/>
        </Button>
        <Button onPress={pay} style={style.buttons}>
            <FontAwesome name="credit-card" size={30} style={style.whiteText}/>
            <Text style={style.whiteText}>Apmokėti</Text>
        </Button>
    </View>
    </Row>
);
let input = "";
inputDialog = (visible, ti, onCodeScanned) => {
    return (
    <Dialog visible={visible} onRequestClose={()=>ti(false)}>
    <Grid>
        <Row style={{height: 80}}>
        <TextInput style={style.input} placeholder="Įveskite prekės kodą" onChangeText={(text) => input = text}/>
        </Row>
        <Row size={70}>
        <Button style={style.buttons} onPress={()=>{
            onCodeScanned(input);
            ti(false);
        }}>
            <MaterialIcons name="add-circle-outline" size={30} style={style.whiteText}/>
            <Text style={style.whiteText}>Pridėti prekę</Text>
        </Button>
        <Button style={style.buttons} onPress={()=>ti(false)}>
            <MaterialCommunityIcons name="cancel" size={30} style={style.whiteText}/>
            <Text style={style.whiteText}>Atšaukti</Text>
        </Button>
        </Row>
    </Grid>
    </Dialog>
    );
}

ShoppingCart.navigationOptions = () => ({
    title: 'Pagrindinis',
});

export default ShoppingCart;