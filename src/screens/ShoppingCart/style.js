import { StyleSheet, Dimensions } from 'react-native';
import colors from './../../configs/colors.dark';
let width = Dimensions.get('window').width;

export default StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: colors.primary,
      alignItems: 'center',
      color: colors.textColor,
      justifyContent: 'center',
    },
    body: {
      backgroundColor: colors.primary,
    },
    buttons:{
      flex: 1,
      marginTop: 5,
      alignItems: 'center',
      paddingHorizontal: 10,
      color: '#ffffff',
      justifyContent: 'center',
    },
    centerButton:{
      flex: 0,
      backgroundColor: colors.buttonColor,
      borderRadius: 10,
      padding: 5,
      marginHorizontal: 10,
      marginTop: -30
    },
    footer: {
      flex: 1,
      flexWrap: 'wrap', 
      alignItems: 'flex-start',
      flexDirection:'row',
      backgroundColor: colors.secondary,
      justifyContent: 'center',
    },
    itemRow:{
      height: 40,
      color: '#ffffff',
      alignItems: 'center',
      borderBottomWidth: 1,
      backgroundColor: 'rgba(255,255,255,.08)',
      borderBottomColor: 'rgba(255,255,255,.3)'
    },
    itemCol:{
      alignItems: 'center',
      justifyContent: 'center'
    },
    rowText: {
      color: colors.textColor,
      alignItems: 'center'
    },
    whiteText:{
      color: colors.textColor
    },
    input: {
      width: width - 100,
      borderBottomColor: '#687989',
      backgroundColor: 'rgba(255,255,255,.1)',
      borderBottomWidth: 2,
      color: colors.textColor,
      margin: 20,
      padding: 5
    },
    cartIcon: {
      color: colors.link
    }
  });
  