import React from 'react';
import style from '../style';
import { Text, View } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import SpinningIcon from '../../../components/SpinningIcon';
import Button from '../../../components/Button';
import { Col, Row } from "react-native-easy-grid";
import { ScrollView } from 'react-native-gesture-handler';

const Cart = ({
    remove,
    newCart
}) => {
    if(cart == -1) return cartLoading();
    else if(cart == 0) return cartStart();
    else
        return cartView();
};

renderCartItems = () => {
    return cartItems.map(itemInfo => (
        <Row key={itemInfo.id} style={style.itemRow}>
            <Col size={15} style={style.itemCol}><Text style={style.rowText}>{(itemInfo.amount).toString()}</Text></Col>
            <Col size={50} style={style.itemCol}><Text style={style.rowText}>{itemInfo.name}</Text></Col>
            <Col size={20} style={style.itemCol}><Text style={style.rowText}>{(itemInfo.price*itemInfo.amount).toFixed(2)} €</Text></Col>
            <Col size={10} style={style.itemCol}>
                <FontAwesome onPress={()=>remove(itemInfo.id)} name="remove" size={24} style={{color: '#28D8A1'}} />
            </Col>
        </Row>
    ));
}
totalPrice = () => {
    let price = 0.0;
    cartItems.map(itemInfo =>{
        price += (itemInfo.price*itemInfo.amount);
    });
    return price.toFixed(2);
}
cartView = () => (
    <ScrollView contentContainerStyle={style.scrollableContainer}>
        <Row  style={style.itemRow}>
            <Col size={15} style={style.itemCol}><Text style={style.rowText}>Kiekis</Text></Col>
            <Col size={50} style={style.itemCol}><Text style={style.rowText}>Produkto pavadinimas</Text></Col>
            <Col size={20} style={style.itemCol}><Text style={style.rowText}>Kaina</Text></Col>
            <Col size={10} style={style.itemCol}></Col>
        </Row>
        {renderCartItems()}
        <Row  style={style.itemRow}>
            <Col size={15} style={style.itemCol}><Text style={style.rowText}>Viso:</Text></Col>
            <Col size={50} style={style.itemCol}></Col>
            <Col size={20} style={style.itemCol}><Text style={style.rowText}>{totalPrice()} €</Text></Col>
            <Col size={10} style={style.itemCol}></Col>
        </Row>
    </ScrollView>
);

cartStart = () => (
    <View style={style.container}>
        <Row size={30} style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'center',
        }}>
            <Text style={[style.whiteText,{
                marginTop: '40%'
            }]}>Aktyvaus apsipirkimo šiuo metu nėra.</Text> 
        </Row>
        <Row size={70}>
        <Button onPress={newCart} style={style.buttons}>
            <FontAwesome name="shopping-cart" size={128} style={style.cartIcon}/>
            <Text style={style.whiteText}>Pradėti apsipirkimą</Text>
        </Button>
        </Row>
    </View>
);

cartLoading = () => (
    <View style={style.container}>
        <SpinningIcon name="spinner" size={128} style={{ 
            color: '#28D8A1'
        }} />
    </View>
);

export default Cart;