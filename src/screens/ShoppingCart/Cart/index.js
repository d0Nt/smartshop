import { compose, hoistStatics, withProps} from 'recompose';
import Cart from './CartView';

const enhancer = compose(
  withProps(props => {
    navigation = props.navigation,
    cart = props.cart,
    cartItems = props.cartItems,
    remove = (id) => props.removeItem(id),
    newCart = () => props.startNewCart()
  })
);
export default hoistStatics(enhancer)(Cart);