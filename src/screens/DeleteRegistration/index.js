import { compose, hoistStatics, withHandlers, withProps, withState } from 'recompose';
import DeleteRegistration from './DeleteRegistrationView';
import message from './../../components/ToastMessage';
import userVariables from './../../controllers/userVariables';
import Account from './../../controllers/Account';

const enhancer = compose(
  withState('saving', 'setSavingState', false),
  withState('confirmText', 'setConfirmText',''),
  withProps(props => {
    navigation = props.navigation
  }),
  withHandlers({
    submit: props => async () => {
      if(props.saving) return;
      let user = userVariables.getData();
      if(!props.confirmText || props.confirmText != (user.firstName+' '+user.lastName)){
        message("Patvirtinimo kodas neteisingas.");
        return;
      }
      let response = await Account.deleteAccount();
      if(!response){
        message("Registracijos ištrynimas nepavyko.");
        return;
      }
      props.navigation.navigate('Auth');
      message("Registracija bus ištrinta po 30 dienų. Per tą laik nesijunkite prie registracijos, nes ištrynimas bus atšauktas.");
    }
  })
);
export default hoistStatics(enhancer)(DeleteRegistration);
