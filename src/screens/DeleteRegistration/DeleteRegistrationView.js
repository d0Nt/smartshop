import React from 'react';
import style from './style';
import { Text, View, TextInput, ScrollView } from 'react-native';
import { Row, Grid } from "react-native-easy-grid";
import sizes from './../../configs/sizes';
import Button from './../../components/Button';
import Header from './../../components/Header';

const DeleteRegistration = ({
    setConfirmText,
    submit
}) => (
    <Grid>
        <Row size={sizes.headerHeight}>
            <Header title='Regitracijos ištrynimas' nav={navigation}/>
        </Row>
        <Row style={style.body} size={sizes.bodyHeight}>
            <ScrollView contentContainerStyle={style.container}>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>IŠTRINANT REGISTRACIJĄ:</Text>
                    <Text style={style.text}>• ji bus ištrinta po 30 dienų, jei neprisijungsite;{"\n"}
                    • prisijungus per 30 dienų laikotarpį, ištrynimas atšaukiamas;{"\n"}
                    • po 30 dienų visi pirkimų duomenys bus nuasmeninti, o registracija ištrinta;{"\n"}
                    • norint ištrinti registraciją reikia įvesti savo vardą ir pavardę į lauką žemiau.{"\n"}</Text>
                </View>

                <View style={style.settingContainer}>
                    <Text style={style.settingTitle}>PATVIRTINIMAS</Text>
                    <TextInput style={style.input} onChangeText={(val) => setConfirmText(val)} placeholder='Vardas Pavardė'/>
                </View>

                <Button titleStyle={style.text} title="Patvirtinti" style={[style.button, style.buttonSave]} onPress={submit}/>
            </ScrollView>
        </Row>
    </Grid>
);

DeleteRegistration.navigationOptions = () => ({
    title: 'Slaptažodžio keitimas',
    header: null
});

export default DeleteRegistration;