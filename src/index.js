import React from 'react';
import Navigation from './navigation';
import { AppLoading } from 'expo';

export default class App extends React.Component {
  state = {
      loading: true
  }
  render() {
    if(this.state.loading)
      return <AppLoading
        startAsync={()=>{}}
        onFinish={() =>this.setState({loading: false})}
        onError={console.warn}
      />;

    return <Navigation />;
  }
}