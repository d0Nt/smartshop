import {SecureStore} from 'expo';
import userVariables from '../userVariables';

export default {
    load: async () => {
        let id = await SecureStore.getItemAsync("id")
        .then(val => val).catch(() => null);
        let token = await SecureStore.getItemAsync("token")
        .then(val => val).catch(() => null);
        await new Promise(resolve => setTimeout(resolve, 100));
        if(!id)
            return null;
        userVariables.setData({
            id: id,
            token: token
        });
        return {
            userId: id,
            token: token
        };
    },
    save: (id, token) => {
        userVariables.setData({
            id: id,
            token: token
        });
        SecureStore.setItemAsync("id", id.toString());
        SecureStore.setItemAsync("token", token);
    },
    delete: () => {
        SecureStore.deleteItemAsync("id");
        SecureStore.deleteItemAsync("token");
    }
}