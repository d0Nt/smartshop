import validations from './Validations';
import registration from './Registration';

export default {
    validateMail: validations.validateMail,
    validatePassword: validations.validatePassword,
    restoreSession: registration.restore,
    register: registration.register,
    login: registration.login,
    logout: registration.logout,
    resetPassword: registration.resetPassword,
    changePassword: registration.changePassword,
    getData: registration.getData,
    saveUserData: registration.saveData,
    deleteAccount: registration.delete
}