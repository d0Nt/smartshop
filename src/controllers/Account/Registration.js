import Requests from './../Requests';
import Session from './Session';
import Validation from './Validations';
import userVariables from '../userVariables';

export default {
    register: async (data) => {
        if(!data.firstName){
            return false;
        }
        if(!data.lastName){
            return false;
        }
        if(!data.email){
            return false;
        }
        if(!Validation.validateMail(data.email)){
            return false;
        }
        if(!data.password){
            return false;
        }
        if(!Validation.validatePassword(data.password)){
            return false;
        }
        if(data.password != data.passwordAgain){
            return false;
        }
        let response = await Requests.post("/user/register", data);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return true;
    },
    restore: async () => {
        let data = await Session.load();
        if(!data) return false;
        let response = await Requests.post("/user/check", {
            "user_id": data.userId,
        }, data.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        //if token changed then save new here
        return true;
    },
    login: async (mail, password) => {
        let response = await Requests.post("/user/login", {
            "email": mail, 
            "password": password
        });
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        Session.save(response.id, response.token);
        return true;
    },
    logout: () => {
        Session.delete();
    },
    restore: async () => {
        let data = await Session.load();
        if(!data) return false;
        let response = await Requests.post("/user/check", {
            "user_id": data.userId,
        }, data.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        //if token changed then save new here
        return true;
    },
    delete: async () => {
        let user = userVariables.getData();
        let response = await Requests.post("/user/delete", {
            'user_id': user.id
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        Session.delete();
        return true;
    },
    getData: async() => {
        let user = userVariables.getData();
        let response = await Requests.post("/user/getData", {
            "user_id": user.id
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        userVariables.setData(response);
        return response;
    },
    saveData: async(data) => {
        let user = userVariables.getData();
        data.user_id = user.id;
        let response = await Requests.post("/user/saveData", data, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        userVariables.setData(response);
        return response;
    },
    resetPassword: async (email) => {
        if(!email){
            return false;
        }
        if(!Validation.validateMail(email)){
            return false;
        }
        let response = await Requests.post("/user/requestResetPassword", {
            "email": mail
        });
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return true;
    },
    changePassword: async (old, newPass, newAgain) => {
        if(newPass !== newAgain) return false;
        let user = userVariables.getData();
        let response = await Requests.post("/user/changePassword", {
            "user_id": user.id,
            "old": old,
            "new": newPass
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return true;
    }
}