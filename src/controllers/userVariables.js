initialise = () => {
    if(typeof global.user === 'undefined'){
        global.user = {
            id: 0,
            token: ''
        };
    }
}

export default {
    getData: () =>{
        initialise();
        return {...global.user}
    },
    setData: (updateValues) => {
        initialise();
        Object.assign(global.user, updateValues);
    }
}