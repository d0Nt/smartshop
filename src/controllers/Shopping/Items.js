import Requests from './../Requests';
import userVariables from '../userVariables';
export default {
    list: async (cart_id) => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/itemsList/", {
            "user_id": user.id,
            "cart_id": cart_id
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return response;
    },
    remove: async(cart_id, item_id) => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/removeItem/", {
            "user_id": user.id,
            "item_id": item_id,
            "cart_id": cart_id
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return response;
    },
    add: async (item_code, cart_id) => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/addItem/", {
            "user_id": user.id,
            "item_code": item_code,
            "cart_id": cart_id
        }, user.token);
        if(response == null){
            return {
                response: null,
                message: ""
            };
        }
        if(response.error){
            console.log(response.error);
            return {
                response: false,
                message: response.error
            };
        }
        return {
            response: response,
            message: response.error
        };
    }
}