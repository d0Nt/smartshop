import Requests from '../Requests';
import userVariables from '../userVariables';

export default {
    create: async () => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/create", {
            "user_id": user.id, 
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return response.cart_id;
    },
    getActive: async () => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/getActive", {
            "user_id": user.id, 
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return response.cart_id;
    },
    pay: async (cartId) => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/pay", {
            "user_id": user.id, 
            "cart_id": cartId
        }, user.token);
        if(response == null){
            return null;
        }
        return response;
    },
    hisotry: async () => {
        let user = userVariables.getData();
        let response = await Requests.post("/cart/history", {
            "user_id": user.id, 
        }, user.token);
        if(response == null){
            return null;
        }
        if(response.error){
            return false;
        }
        return response;
    }
}