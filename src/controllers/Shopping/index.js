import Cart from "./Cart";
import Items from "./Items";

export default {
    getActiveCart: Cart.getActive,
    getCartItems: Items.list,
    removeItem: Items.remove,
    addItem: Items.add,
    purchaseHistory: Cart.hisotry,
    createCart: Cart.create,
    payForCart: Cart.pay
}