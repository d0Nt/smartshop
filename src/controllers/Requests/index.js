import config from './../../configs/settings';
export default{
    post: async (api, data, token) => {
        let headers = {
            Accept: 'application/json',
            'Content-Type': 'application/json'
        };
        if(typeof token != 'undefined')
            headers["authorization"] = 'Token token="'+token+'"';
        if(typeof data == 'undefined')
            data = [];
        return await fetch(config.api_url+''+api, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(data),
        }).then(data => data.json()).catch(reason =>{
            console.log("error: " + reason);
            return null;
        });
    }
}