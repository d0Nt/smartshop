export default {
    primary: "#1C1B21",
    secondary: "#262630",
    iconColor: "#ffffff",
    textColor: "#ffffff",
    buttonColor: "#28D8A1",
    link: "#28D8A1"
}