import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BarCodeScanner, Permissions } from 'expo';
import Button from './Button';
import SpinningIcon from './SpinningIcon';


export default class BarcodeScannerExample extends React.Component {
    state = {
        hasCameraPermission: null,
    }

    async componentDidMount() 
    {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
        this.handler = () => {};
        const { onCodeScanned } = this.props;
        if(typeof onCodeScanned !== 'undefined')
            this.handler = onCodeScanned;
    }
    render() {
        const { hasCameraPermission } = this.state;
        const { nav } = this.props;
        if (hasCameraPermission === null) {
        return (
            <View style={style.container}>
              <SpinningIcon name="spinner" size={128} style={{ 
                  color: '#28D8A1'
              }} />
            </View>
          );
        }
        if (hasCameraPermission === false) {
          return (
            <View style={style.container}>
              <Text>Nesuteiktas leidimas kamerai</Text>
            </View>
          );
        }
        return (
        <View style={{ flex: 1 }}>
            <BarCodeScanner
            onBarCodeScanned={this.handleBarCodeScanned}
            style={StyleSheet.absoluteFill}
            />
            <View style={[style.overlay, style.bottomOverlay]}>
                <Button
                    onPress={() => this.handler(null)}
                    style={style.backButton}
                    title="Grįžti" />
            </View>

        </View>
        );
    }
    handleBarCodeScanned = ({ type, data }) => {
        this.handler(data);
    }
}
import colors from './../configs/colors.dark.js';
const style = {
      container: {
        flex: 1,
        backgroundColor: colors.primary,
        alignItems: 'center',
        color: colors.textColor,
        justifyContent: 'center'
    },
    overlay: {
      position: 'absolute',
      padding: 16,
      right: 0,
      left: 0,
      alignItems: 'center'
    },
    topOverlay: {
      top: 0,
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center'
    },
    bottomOverlay: {
      bottom: 0,
      backgroundColor: 'rgba(0,0,0,0.4)',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center'
    },
    backButton: {
      backgroundColor: 'white',
      borderRadius: 40
    }
  };
  