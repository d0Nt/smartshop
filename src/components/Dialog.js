import React, { Component } from 'react';
import { View, Text, Modal, TouchableHighlight } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import colors from './../configs/colors.dark';

export default class Dialog extends Component {
  render() {
      const {visible, children, onRequestClose, style } = this.props;
    return (
    <Modal
        animationType="fade"
        transparent={true}
        visible={visible}
        onRequestClose={onRequestClose}
        >
        <View style={{
            height: '100%',
            width: '100%',
            backgroundColor: 'rgba(0, 0, 0, 0.5)',
            alignItems: 'center',
            justifyContent: 'center'
        }}>
            <View style={[{
                minHeight: 150,
                minWidth: '50%',
                backgroundColor: colors.secondary,
            },style]}>
              {children}
            </View>
        </View>
    </Modal>
    );
  }
}