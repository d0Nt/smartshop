import React, { Component } from 'react';
import { Text, TouchableOpacity } from 'react-native';

export default class Button extends Component {
  render() {
    const { title, disabled, children, onPress, style, titleStyle} = this.props;
    return (
      <TouchableOpacity disabled={disabled} style={style} onPress={onPress}>
        {typeof title !== 'undefined' && <Text style={[{margin: 16, fontWeight: 'bold', color: "#000000"},titleStyle]}>{title}</Text>}  
        {children}
      </TouchableOpacity>
    );
  }
}