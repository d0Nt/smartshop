import React, { Component } from 'react';
import { View, TouchableOpacity, BackHandler, Alert, Text } from 'react-native';
import { FontAwesome } from '@expo/vector-icons';
import colors from './../configs/colors';
import { Col, Row } from "react-native-easy-grid";

let backDefault = (nav) =>{
  if(nav)
    nav.goBack(null);
  else{
    Alert.alert(
      'Išjungti programėlę',
      'Ar tikrai norite išjungti programėlę?',
      [
        {text: 'Ne', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'Taip', onPress: () => BackHandler.exitApp()},
      ],
      { cancelable: false });
  }
}

export default class Header extends Component {
  state = {
    
  }

  render() {
    let { backEvent } = this.props;
    let paddingTop = 20;
    if(!backEvent) backEvent = backDefault;
    return (
      <Row style={{
        backgroundColor: colors.secondary,
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
        display: 'flex'
      }}>
      <Col size={15} style={{
        paddingTop: paddingTop
      }}>
      {!this.props.initial &&
          <TouchableOpacity onPress={()=>backEvent(this.props.nav)}>
            <FontAwesome name="arrow-left" size={25} style={{ 
              color: '#28D8A1'
              }} />
          </TouchableOpacity>
          }
      {this.props.drawerToggle &&
          <TouchableOpacity onPress={()=>this.props.nav.toggleDrawer()}>
            <FontAwesome name="navicon" size={25} style={{ 
              color: '#28D8A1'
              }} />
          </TouchableOpacity>
          }
      </Col>
      <Col size={85} style={{
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: paddingTop
      }}>
      {this.props.title && <Text style={{
        color: colors.textColor,
        fontSize: 18,
        marginLeft: '-15%'
      }}>{this.props.title}</Text>}
      </Col>
      </Row>
    );
  }
}