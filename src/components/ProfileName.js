import React, { Component } from 'react';
import { View, Text } from 'react-native';
import userVariables from '../controllers/userVariables';
import colors from './../configs/colors';

export default class ProfileName extends Component {
  render() {
    let user = userVariables.getData();
    const {style} = this.props;
    return (
      <View style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <Text style={[{color: colors.textColor, marginBottom: 10}, style]}>{user.firstName} {user.lastName}</Text>
      </View>
    );
  }
}