import React, { Component } from 'react';
import {Animated, Easing} from 'react-native';

export default class Fade extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: props.visible,
      duration: typeof props.duration === 'undefined'? 500: props.duration,
      easing: typeof props.easing === 'undefined'? Easing.linear: props.easing
    };
  };

  componentWillMount() {
    this._visibility = new Animated.Value(this.props.visible ? 1 : 0);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.visible) {
      this.setState({ visible: true });
    }
    Animated.timing(this._visibility, {
      toValue: nextProps.visible ? 1 : 0,
      duration: this.duration,
      easing: this.easing
    }).start(() => {
      this.setState({ visible: nextProps.visible });
    });
  }

  render() {
    const { visible, style, children, ...rest } = this.props;

    const containerStyle = {
      opacity: this._visibility.interpolate({
        inputRange: [0, 1],
        outputRange: [0, 1],
      })
    };

    const combinedStyle = [containerStyle, style];
    return (
      <Animated.View style={this.state.visible ? combinedStyle : containerStyle} {...rest}>
        {children}
      </Animated.View>
    );
  }
}