import React, { Component } from 'react';
import { View, Image } from 'react-native';

export default class ProfilePhoto extends Component {
  render() {
    const {style} = this.props;
    return (
      <View style={{
        justifyContent: 'center',
        alignItems: 'center',
      }}>
        <Image source={require('./../../assets/default_avatar.png')} style={[{
          marginBottom: 20,
          width: 150, 
          height: 150, 
          borderRadius: 150 / 2 
        }, style]}/>
      </View>
    );
  }
}